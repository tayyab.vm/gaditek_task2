<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\Models\User;
        $user->name = 'Tayyab';
        $user->email = 'tayyab@gmail.com';
        $user->password = bcrypt('tayyab123');
        $user->save();

        $user = new \App\Models\User;
        $user->name = 'Faran';
        $user->email = 'faran@gmail.com';
        $user->password = bcrypt('faran123');
        $user->save();

        $user = new \App\Models\User;
        $user->name = 'Khurram';
        $user->email = 'khurram@gmail.com';
        $user->password = bcrypt('khurram123');
        $user->save();
    }
}
