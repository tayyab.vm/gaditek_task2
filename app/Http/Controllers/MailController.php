<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Email;
use App\Models\User;
use App\Models\Recipient;
use App\Models\Reply;
class MailController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    
    public function inbox(){
        $emails = Recipient::where('recipient_id',\Auth::user()->id)->simplePaginate(10);
        return view('mail.inbox')->with('emails',$emails);
    }
    
    
    public function create(){
        return view('mail.create');
    }
    
    public function send(){
        request()->validate([
            'recipients' => 'required',
            'subject' => 'required',
            'body' => 'required',
        ]);
        try{
            $email = new Email;
            $email->subject = request()->subject;
            $email->body = request()->body;
            $email->last_reply = request()->body;
            $email->last_reply_from = 0;
            $email->sender_id = \Auth::user()->id;
            $email->save();
            foreach(request()->recipients as $recipient){
                $user = User::where('email',$recipient)->first();
                if($user){
                    $recipient = new Recipient;
                    $recipient->email_id = $email->id;
                    $recipient->sender_id = \Auth::user()->id;
                    $recipient->recipient_id = $user->id;
                    $recipient->save();
                }
            }
        }catch(\Exception $e){
            \Session::flash('error','Something Went Wrong!');
            return back();
        }
        
        \Session::flash('success','Email Sent Successfully!');
        return redirect()->route('home');
    }
    
    public function show($id){
        $email = Email::find($id);
        $replies = Reply::where("email_id",$id)->get();
        return view('mail.show')->with('email',$email)->with('replies',$replies);
    }
    
    public function reply($id){
        request()->validate([
            'reply_body' => 'required',
        ]);
        try{
            $email = Email::find($id);
            
            $reply = new Reply;
            $reply->email_id = $id;
            $reply->from_id = \Auth::user()->id;
            $reply->body = request()->reply_body;
            $reply->save();
            
            $email->last_reply = request()->reply_body;
            $email->last_reply_from = \Auth::user()->id;
            $email->save();
            
            $recipient = Recipient::where('email_id',$email->id)->where('recipient_id',$email->sender_id)->first();
            if(!$recipient){
                $recipient = new Recipient;
                $recipient->email_id = $id;
                $recipient->sender_id = \Auth::user()->id;
                $recipient->recipient_id = $email->sender_id;
                $recipient->save();
            }
        }
        catch(\Exception $e){
            \Session::flash('error','Something Went Wrong!');
            return back();
        }
        \Session::flash('success','Email Sent Successfully!');
        
        return back();
    }
    
    public function sent(){
        $emails = Email::where('sender_id',\Auth::user()->id)->paginate(10);
        return view('mail.sent')->with('emails',$emails);
    }
    
    
}

