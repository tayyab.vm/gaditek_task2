<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recipient extends Model
{
    use HasFactory;


    public function email(){
        return $this->belongsTo('App\Models\Email','email_id');
    }

    public function sender(){
        return $this->belongsTo('App\Models\User','sender_id');
    }

    public function name(){
        return $this->belongsTo('App\Models\User','recipient_id');
    }
}
