<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    use HasFactory;

    public function sender(){
        return $this->belongsTo('App\Models\User','sender_id');
    }
    
    public function recipients(){
        return $this->hasMany('App\Models\Recipient');
    }

    public function replies(){
        return $this->hasMany('App\Models\Reply');
    }

    public function lastReply(){
        if($this->replies()->exists()){
            // dd('');
            return $this->replies();
        }
        return 'hit';
    }

    public function lastReplyFrom(){
        return $this->belongsTo('App\Models\User','last_reply_from');
    }

    // public function reply_count(){
    //     return $this->where('parent_id',$this->getOriginal('id'))->get()->count() == 0 ? '' : $this->where('parent_id',$this->getOriginal('id'))->get()->count();
    // }

  
}
