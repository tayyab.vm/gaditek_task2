@extends('auth.layouts.master')

@section('main-content')
<form action="/login" method="post">
    @csrf
    <img class="mb-4" src="{{asset('images/loginlogo.png')}}" alt="" width="72" height="57">
    <h1 class="h3 mb-3 fw-normal">Please sign in</h1>
    @foreach ($errors->all() as $error)
    <div class="alert alert-danger" role="alert">
        {{$error}}
    </div>
    @endforeach
    <div class="form-floating">
        <input type="email" class="form-control" id="floatingInput" placeholder="Your Email" name="email">
        <label for="floatingInput">Email address</label>
    </div>
    <div class="form-floating">
        <input type="password" class="form-control" id="floatingPassword" placeholder="Password" name="password">
        <label for="floatingPassword">Password</label>
    </div>
    
    <div class="checkbox mb-3">
        <label>
            <a href="/register">Register</a>
            {{-- <input type="checkbox" value="remember-me"> Register --}}
        </label>
    </div>
    <button class="w-100 btn btn-lg btn-primary" type="submit">Sign in</button>
    <p class="mt-5 mb-3 text-muted">&copy; 2017–2021</p>
</form>
@endsection