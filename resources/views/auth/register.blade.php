@extends('auth.layouts.master')

@section('main-content')
<form action="/register" method="post">
      
  @csrf
  <img class="mb-4" src="{{asset('images/loginlogo.png')}}" alt="" width="72" height="57">
  <h1 class="h3 mb-3 fw-normal">Please sign in</h1>
  @foreach ($errors->all() as $error)
  <div class="alert alert-danger" role="alert">
    {{$error}}
  </div>
  @endforeach
  <div class="form-floating">
    <input type="text" class="form-control" id="floatingInput" placeholder="Your Name" name="name" style="margin-bottom:-1px">
    <label for="floatingInput">Name</label>
  </div>
  <div class="form-floating">
    <input type="email" class="form-control" id="floatingInput" placeholder="Your Email" name="email">
    <label for="floatingInput">Email address</label>
  </div>
  <div class="form-floating">
    <input type="password" class="form-control" id="floatingInput" placeholder="Password" name="password" style="margin-bottom:-1px">
    <label for="floatingInput">Password</label>
  </div>
  <div class="form-floating">
    <input type="password" class="form-control" id="floatingInput" placeholder="Confirm Password" name="password_confirmation">
    <label for="floatingInput">Confirm Password</label>
  </div>
  
  <div class="checkbox mb-3">
    <label>
      <a href="/login">Login</a>
      {{-- <input type="checkbox" value="remember-me"> Register --}}
    </label>
  </div>
  <button class="w-100 btn btn-lg btn-primary" type="submit">Register</button>
  <p class="mt-5 mb-3 text-muted">&copy; 2017–2021</p>
</form>
@endsection