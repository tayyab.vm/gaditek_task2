@extends('layouts.master')

@section('page-title')
New Email
@endsection
@section('main-content')
<div class="col">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger" role="alert">
        {{Session::get('error')}}
    </div>
    @endif
    <form action="{{route('send')}}" method="post">
        @csrf
        <div class="form-group">
            <label for="control-label">To</label> 
            <select class="js-example-basic-multiple form-control" name="recipients[]" multiple="multiple">
            </select>
        </div>
        
        <div class="form-group">
            <label for="control-label">Subject</label>
            <input type="text" class="form-control" name="subject">
        </div>
        <div class="form-group">
            <label for="control-label">Message</label>
            <textarea class="form-control mail_body" name="body"></textarea>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-success mt-3" value="Send">
        </div>
    </form>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2({
            tags: true,
        });
    });
</script>
@endsection