@extends('layouts.master')

@section('page-title')
Sent
@endsection
@section('main-content')
<div class="table-responsive">
  {{$emails->links()}}
  
  <table class="table">
    <thead>
    </thead>
    @foreach ($emails as $email)
    <tr>
      <td>
        <p class="subject">To: 
          @foreach ($email->recipients as $index => $recipient)
          @if ($index <= 1)
          {{$recipient->name->name}},
          @endif
          @endforeach   
          <span class="recipients_count"></span></p>
        </td>
        <td>
          <p class="body"> <span class="body_subject"> {{$email->subject}}</span> - <span class="body_body"> {{$email->body}}</span></p>
        </td>
        <td>
          <a href="{{route('show',$email->id)}}">View</a>
        </td>
      </tr>
      @endforeach
    </table>
  </div>
  @endsection