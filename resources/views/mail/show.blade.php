@extends('layouts.master')

@section('page-title')
Inbox
@endsection
@section('main-content')
<div class="table-responsive">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{Session::get('success')}}
    </div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger" role="alert">
        {{Session::get('error')}}
    </div>
    @endif
    <div class="col">
        <h4>{{$email->subject}}</h4>
    </div>
    <br>
    
    <div class="accordion" id="accordionExample">
        
        <div class="accordion-item">
            <h2 class="accordion-header" id="headingThree">
                <div class="row accordion-button {{$replies->count() == 0 ? '' : 'collapsed'}}" data-bs-toggle="collapse" data-bs-target="#collapseone" aria-expanded="true" aria-controls="collapseone">
                    <div class="col">
                        <span class="sender_name">{{$email->sender->name}}</span><br>
                        <span class="body"> <span class="email_body"> {{substr($email->body,0,100)}}...</span>
                    </div>
                </div>
                
            </h2>
            <div id="collapseone" class="accordion-collapse collapse {{$replies->count() == 0 ? 'show' : ''}}" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                    <p class="body">  <span class="body_body"> {{$email->body}}</span></p>
                </div>
            </div>
        </div>
    </div>
    @foreach ($replies as $index => $reply)
    
    <div class="accordion" id="accordionExample">
        
        <div class="accordion-item">
            <h2 class="accordion-header" id="headingThree">
                <div class="row accordion-button {{$replies->count() == $index+1 ? '' : 'collapsed'}}" data-bs-toggle="collapse" data-bs-target="#collapse{{$reply->id}}" aria-expanded="true" aria-controls="collapse{{$reply->id}}">
                    <div class="col">
                        <span class="sender_name">{{$reply->sender->name}}</span><br>
                        <span class="body"> <span class="email_body"> {{substr($reply->body,0,100)}}...</span>
                    </div>
                </div>
                
            </h2>
            <div id="collapse{{$reply->id}}" class="accordion-collapse collapse {{$replies->count() == $index+1 ? 'show' : ''}}" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                    <p class="body">  <span class="body_body"> {{$reply->body}}</span></p>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    <br>
</div>
<form action="{{route('reply',$email->id)}}" method="post">
    
    <div class="row">
        @csrf
        <div class="col">
            <textarea name="reply_body" class="form-control"></textarea>    
        </div>
    </div>
    <br>
    <button type="submit" class="btn btn-primary">Reply</button>
</form>

@endsection