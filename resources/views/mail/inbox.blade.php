@extends('layouts.master')

@section('page-title')
Inbox
@endsection
@section('main-content')

<div class="table-responsive">
    @if (Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{Session::get('success')}}
    </div>    
    @endif
    {{$emails->links()}}
    
    <table class="table">
        @foreach ($emails as $email)
        <tr>
            <td style="width: 250px">
                <span class="subject">{{$email->email->sender->id == Auth::user()->id ? 'me' : $email->sender->name}} , {{$email->email->lastReplyFrom->name  ?? ''}}
                    
                    {{-- <span class="recipients_count">{{$email->email->reply_count()}}</span> --}}
                </span>
            </td>
            <td>
                <span class="body"> <span class="body_subject"> {{$email->email->subject}}</span> - 
                <span class="body_body"> {{substr($email->email->last_reply,0,100)}}...</span>
                {{-- <span class="body_body"> {{$email->replies}}...</span> --}}
            </span>
        </td>
        <td>
            <a href="{{route('show',$email->email->id)}}">View</a>
        </td>
    </tr>
    @endforeach
</table>
</div>
@endsection