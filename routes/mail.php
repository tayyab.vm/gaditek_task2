<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\MailController@inbox')->name('home');
Route::get('/sent', 'App\Http\Controllers\MailController@sent')->name('sent');
Route::get('/show/{id}', 'App\Http\Controllers\MailController@show')->name('show');
Route::post('/reply{id}', 'App\Http\Controllers\MailController@reply')->name('reply');

Route::get('/create', 'App\Http\Controllers\MailController@create')->name('create');
Route::post('/send', 'App\Http\Controllers\MailController@send')->name('send');
// Route::get('/', 'App\Http\Controllers\MailController@inbox');
